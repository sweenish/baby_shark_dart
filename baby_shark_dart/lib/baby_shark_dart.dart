import 'package:meta/meta.dart';

List<String> _family = const [
  'Baby',
  'Mommy',
  'Daddy',
  'Grandma',
  'Grandpa'
];

List<String> _action = const [
  'Let\'s go hunt',
  'Run away',
  'Safe at last',
  'It\'s the end'
];

String _doo = 'doo doo doo doo doo doo';

void sing() {
  _makeChunkOfSong(words: _family, sharks: true);
  _makeChunkOfSong(words: _action, sharks: false);
}

void _makeChunkOfSong({@required List<String> words, @required bool sharks}) {
  for (var word in words) {
    for (int i = 0; i < 3; i++) {
      print('$word${_getPiece(sharks: sharks)} $_doo');
    }
    print('$word${_getPiece(sharks: sharks)}!\n');
  }
}

String _getPiece({@required bool sharks}) {
  return sharks ? ' shark' : '!';
}